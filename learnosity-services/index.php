<?php

ini_set('display_errors', 'on');

include_once("credentials.php");
require 'aws/aws-autoloader.php';
require 'vendor/autoload.php';

use OpenCloud\Rackspace;


/*post vars used by multiple clauses*/
$hash =  filter_input(INPUT_POST, 'md5', FILTER_SANITIZE_STRING);
$text =  filter_input(INPUT_POST, 'text', FILTER_SANITIZE_STRING);

/*post vars for logging a missing resording*/
$log_missing = filter_input(INPUT_POST, 'log_missing_file', FILTER_SANITIZE_STRING);

$activity = filter_input(INPUT_POST, 'activity', FILTER_SANITIZE_STRING);
$voiceID = filter_input(INPUT_POST, 'voiceID', FILTER_SANITIZE_STRING);

/*post vars for request for polly audio file*/
$need_polly = filter_input(INPUT_POST, 'request_polly_audio', FILTER_SANITIZE_STRING);
$language = filter_input(INPUT_POST, 'request_polly_audio', FILTER_SANITIZE_STRING);


if(isset($log_missing)&&(!empty($log_missing)))
{
    if($log_missing === "learnosity")
        $sourceTag = "Learnosity Activity-";
    else if($log_missing === "ebook")
        $sourceTag = "Ebook - ";


        $pdo = pdo_init();


    /*TODO:  re-hash the string to see if the md5 matches?*/
    addHashTextToDB($pdo, $hash, $text, $sourceTag . $activity); /* add hash/text to table audio_hash */

    addNeededAudioFileToLog($pdo, $hash, $voiceID); /* log required audio hash to  audio_files_needed */

}

/*get audio from polly*/
if((isset($need_polly))&&(!empty($need_polly)))
{
    


/************************rackspace API initialization*************************/
$rsClient = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array('username' => $rackspace_user,'apiKey' => $rackspace_api_key )); 
$objectStoreService = $rsClient->objectStoreService(null, 'ORD');
$audioContainer = $objectStoreService->getContainer('big_ideas_math_audio');
$audioContainer->enableCdn();           //make publicly available

/*allw for temp url from rackspace (for new polly recordings only)*/
$account = $objectStoreService->getAccount();
$account->setTempUrlSecret();



/******************AWS Polly API init*******************/
$credentials    = new \Aws\Credentials\Credentials($awsAccessKeyId, $awsSecretKey);
$pollyClient         = new \Aws\Polly\PollyClient([    
    'version' => 'latest',    
    'credentials' => $credentials,    
    'region' => 'us-west-2'
]);


$polly_voice_id = ($language === 'e') ? "Joanna" : "Penelope";

    /*retrieve file*/
    $response = polly_create_recording($text, $polly_voice_id, $pollyClient); 

    /*generate CDN name*/
    $dir  = $hash[0]+$hash[1]+$hash[2];
    $cdnPath = $dir."/tts/".$language."/".$hash.".mp3";


    /*upload to rackspace CDN*/
    if(push_file_to_cdn($cdnPath, $response, $audioContainer))
    {
        $url = $audioContainer->getPublicUrl();
        echo '{"status": "success",  "filename": "'.$url.'"}';

    }  
    else
    {
        echo '{"status": "error", "message": "unable to load file to cdn","recordingType": "none", "string": "'.$cdnName.'", "filename": "'.$tempURL.'"}';
    }

}




function pdo_init() {
    


$host = "166.78.103.146";
$db = "audio";
$user = "evotext";
$pass = "Vu1Tkc409s09vV4zkVNF";
$charset = 'utf8';

    try {
        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";


        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        $pdo = new PDO($dsn, $user, $pass, $opt);
        
    } catch (Exception $e) {
        echo $e->getMessage();
        die();
    }
    return $pdo;
}

/* add a new record to the audio_hash table */

function addHashTextToDB($pdo, $hash, $string, $source) {
    $sql = "INSERT IGNORE INTO `audio_hash` (`audio_hash_id`, `audio_text`, `notes`) VALUES (?, ?, ?)";

    $stmt = $pdo->prepare($sql);

    $source = "Source: " . $source;

    $stmt->execute([$hash, $string, $source]);
}


function addNeededAudioFileToLog($pdo, $hash, $preferredVoiceID) {
    $sql = "INSERT IGNORE INTO `audio_files_needed` (`audio_hash_id`, `preferred_audio_voice_id`) VALUES (?,?)";

    $stmt = $pdo->prepare($sql);
    $stmt->execute([$hash, $preferredVoiceID]);
}



function polly_create_recording($text, $voiceID, &$pollyClient)
{
    $url =  $pollyClient->createSynthesizeSpeechPreSignedUrl([
        'OutputFormat' => 'mp3',
        'Text'         => $text,
        'VoiceId'      => $voiceID,
    ]);
    
    

    /*config stream https*/
    $arrContextOptions = array(
        "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
         ),
    );
    /*retrieve file*/
    return file_get_contents($url, false, stream_context_create($arrContextOptions)); 
}


function push_file_to_cdn($cdnName, $tempURL, $container)
{
    /*upload to rackspace CDN*/
    try{
        $custom_headers = array('Access-Control-Allow-Origin' => '*');          //allow JS/ ajax calls to pull url
        $container->uploadObject($cdnName, $tempURL, $custom_headers);

        while(!$container->objectExists($cdnName)){}  //kill time until upload complete

        return true;
         
    }
    catch(Exception $e)
    {
        echo '{"status": "error", "message": "'.$e->getMessage().'","recordingType": "none", "string": "'.$cdnName.'", "filename": "'.$tempURL.'"}';
        exit();
    } 
}
?>