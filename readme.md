initial implementation for serving dynamic audio content to learnosity player through rackspace CDN.


  /*PLEASE USE 
js/bim-cdn.js 
      OR 
dist/bim-cdn-min.js
*/

the cdn-services index.php will remain static on a server, handling interaction with rackspace and AWS polly.  
(current location: www-staging.bigideasmath.com/assessmentSandbox/cdn-player/cdn/index.php)

Client-side script, which we will provide to evotext, will require no dependencies, instead assuming that it's calling page will
provide it with content.  


/***
  index.html || index.php || or whatever page learnosity players are being embedded...
  - hook into their eventOptions object to initialize cdn-audio-text parsing.


/*define the event object to pass to LearnosityItems.init()*/
   var eventOptions = {

     /*here is where we define callback action to take once daata has been received from Learnosity*/
      readyListener: function (e) {


  /*our code to crawl page and insert links for audio*/

           var parser = new LearnosityAudioParser();
           parser.seek_all_text_nodes(true);                                  <-- define seek depth  true | false 
           parser.append_controls(audio_element);                             <--  trigger the creation of audio elemen to play mp3s
           parser.setTarget('lrn_stimulus_content', 'lrn_sharedpassage');     <-- define which learnosity areas areas we want provide audio for 


             /*make sure math quill has finished rendering before we attempt text parsing*/
             /*async action means we need to let mathquill rendering complete before we attempt to process text*/

             var waiting_for_mathquill = setInterval(function () {
                 if ($(".lrn_math_static ").hasClass("lrn_math_renderer_processed")) {
                     clearInterval(waiting_for_mathquill);
                     parser.render_playback_links();     //gather text & create audio playback link
                 } else {
                 }
             }, 50);
**/

As it stands, we shall include a script that hooks into Learnosity initialization through the 'ready listener' hook, which triggers parsing of all learnosity
items' text, generation of md5 hash from that text, and CDN query generation, as well as appending a 'playback button' to each '.learn_stimulus' classed element's
text string.  

We need to work out, with evotext, how the language is being determined, right now, it looks at a set of external (to the learnosity player) controls to determine this.  

additionally, we want to allow for the ability of passing in a 'voice array' of allowed voice ids, this element is crucial to providing access to the biggest potential base of recordings possible, in theory reducing the number of TTS files served to the client.  

If there are learnosity config options that dictate language used, then perhaps look into passing some flag/value in the readylistener hook for our service to determine the language that we wish to use.  

staging url: 
http://www-staging.bigideasmath.com/assessmentSandbox/cdn-player/index.php?itemState=initial&item_ref=+%2219NA00_0100CR_01%22%2C+%2219NA00_0100CR_02%22%2C+%2219NA00_0100CR_03%22%2C+%2219NA00_0100CR_04%22%2C+%2219NA00_0100CR_05%22%2C+%2219NA00_0100CR_06%22%2C+%2219NA00_0100CR_07%22%2C+%2219NA00_0100CR_08%22%2C+%2219NA00_0100CR_09%22%2C+%2219NA00_0100CR_10%22%2C+%2219NA00_0100CR_11%22%2C+%2219NA00_0100CR_12%22%2C+%2219NA00_0100CR_13%22%2C+%2219NA00_0100CR_14%22%2C+%2219NA00_0100CR_15%22%2C+%2219NA00_0100CR_16%22%2C+%2219NA00_0100CR_17%22%2C+%2219NA00_0100CR_18%22++++++&qApi=v2.102&width=&height=


login reports.bim

pass - like kp


/*TODOs*/
~ display some sort of 'loading' icon or graphic to user while CDN is searched?  

 