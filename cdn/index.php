<?php
ini_set('display_errors', 'on');

 header("Access-Control-Allow-Origin: *");
$text = filter_input(INPUT_POST, 'text', FILTER_SANITIZE_STRING);
$action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
$md5 = filter_input(INPUT_POST, 'hash', FILTER_SANITIZE_STRING);

    include_once("credentials.php");
    require './aws/aws-autoloader.php';
    require './vendor/autoload.php';
    use OpenCloud\Rackspace;

if((isset($action))&&(!empty($action))&&($action === "learnosity_tts_audio_only"))
{



    /************************rackspace API initialization*************************/
    $rsClient = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array('username' => $rackspace_user,'apiKey' => $rackspace_api_key )); 
    $objectStoreService = $rsClient->objectStoreService(null, 'ORD');
    $audioContainer = $objectStoreService->getContainer('big_ideas_math_audio');
    $audioContainer->enableCdn();           //make publicly available

    /*allw for temp url from rackspace (for new polly recordings only)*/
    $account = $objectStoreService->getAccount();
    $account->setTempUrlSecret();

    /******************AWS Polly API init*******************/
    $credentials    = new \Aws\Credentials\Credentials($awsAccessKeyId, $awsSecretKey);
    $pollyClient         = new \Aws\Polly\PollyClient([    
        'version' => 'latest',    
        'credentials' => $credentials,    
        'region' => 'us-west-2'
    ]);

    $polly_voice_id =  "Joanna";
    
    /*retrieve file*/
    $response = polly_create_recording($text, $polly_voice_id, $pollyClient); 

    /*generate CDN name*/
    $dir  = $md5[0].$md5[1].$md5[2];
    $cdnPath = $dir."/tts/e/".$md5.".mp3";


    /*upload to rackspace CDN*/
    $url  = push_file_to_cdn($cdnPath, $response, $audioContainer);

    echo '{"status": "success",  "filename": "'.$cdnPath.'"}';
    exit(0);


}
    function polly_create_recording($text, $voiceID, &$pollyClient)
    {
        $url =  $pollyClient->createSynthesizeSpeechPreSignedUrl([
            'OutputFormat' => 'mp3',
            'Text'         => $text,
            'VoiceId'      => $voiceID,
        ]);
        /*config stream https*/
        $arrContextOptions = array(
            "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
             ),
        );
        /*retrieve file*/
        return file_get_contents($url, false, stream_context_create($arrContextOptions)); 
    }

    function push_file_to_cdn($cdnName, $tempURL, &$container)
    {
        /*upload to rackspace CDN*/
        try{
            $custom_headers = array('Access-Control-Allow-Origin' => '*');          //allow JS/ ajax calls to pull url
            $container->uploadObject($cdnName, $tempURL, $custom_headers);

            while(!($container->objectExists($cdnName))){
            
            }  //kill time until upload complete

        }
        catch(Exception $e)
        {
            echo '{"status": "error", "message": "'.$e->getMessage().'","recordingType": "none", "string": "'.$cdnName.'", "filename": "'.$tempURL.'"}';
            exit();
        } 
    }
        

?>