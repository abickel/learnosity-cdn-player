'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var sftp = require('gulp-sftp');
var ts = require('gulp-typescript');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

var minify = require('gulp-minify');


var creds = require("./.creds/larson-staging.json");

/** SFTP Configuration **/
var user = creds.credentials.user;
var password = creds.credentials.pword;
var host = creds.credentials.host;
var port = 21;
var remoteFolder = '/data/inetserv/wwwroot/www.bigideasmath.com/assessmentSandbox/cdn-player';

gulp.task('default', ['sftp-deploy-watch']);

var globs = [
    '!{.git/,.git/**}', //ignore git 
    '!{node_modules/,node_modules/**}', //dont need node packages
    '!{.creds/,.creds/**}', //dont want our credential sup on the server
    '!{.git/,.git/**}',
    '!{.vscode/,.vscode/**}',
    '{*, */**}',
    '*.php',
    'cdn/*.php',
    'dist/*.js',
    'js/*'
];

gulp.task('sftp-deploy-watch', function() {
    gulp.watch(globs)
        .on('change', function(event) {

            console.log('Change/Upload: "' + event.path + '", ' + event.type);
            return gulp.src([event.path], { base: '.', buffer: false })
                .pipe(sftp({
                    host: host,
                    pass: password,
                    user: user,
                    remotePath: remoteFolder
                }));
        });
});



gulp.task('concat-ify', function() {
    return gulp.src('js/*.js')
        .pipe(concat('bim-cdn.js'))
        .pipe(gulp.dest('./dist/'));
});


gulp.task('compress', function() {
    gulp.src('dist/bim-cdn.js')
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '-min.js'
            }
        }))
        .pipe(gulp.dest('dist'))
});