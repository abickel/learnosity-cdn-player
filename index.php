<?php
session_start();
$_SESSION['LastAttemptedPage'] = "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
if (!isset($_SESSION["loggedin"]) or ( $_SESSION["loggedin"] == false)) {
    header('Location: login.php');
    exit("please login first");
}
?>
<?php
/* SFTP!~~ */

use LearnositySdk\Request\Init;
use LearnositySdk\Utils\Uuid;

include_once '../learnosityFiles/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Big Ideas Math - Tutor Interface - Learnosity Problems</title>
        <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>        <!-- <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script> -->
        <script src="//items.learnosity.com"></script>
        <link rel="stylesheet" type="text/css" href="../css/bi-styles.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <script src="js/bim-cdn.js"></script>

        <link rel="stylesheet" type="text/css" href="player.css">
        <script>


            $(document).ready(function () {


                if (!("autofocus" in document.createElement("input"))) {
                    $("#item_ref").focus();
                }

                document.getElementById('validatequestions').onclick = function () {
                    itemsApp.validateQuestions();
                };
                var itemarrayString = '<?php echo $_GET['item_ref']; ?>';
                var itemarray = itemarrayString.split(',');
                //<?php
if (isset($_GET['item_ref'])) {
    echo "LoadQuestionSkillData(itemarray, 'skillResults');";
}
?>

            });


        </script>
        <?php
        $width = isset($_GET['width']) && $_GET['width'] != "" ? $_GET['width'] : "942";
        $height = isset($_GET['height']) && $_GET['height'] != "" ? $_GET['height'] : "544";
        ?>
        <style>
            #viewWindow {
                width:<?php echo $width; ?>px;
                height: <?php echo $height; ?>px;
                overflow: hidden;
                border: solid black 1px;
                margin:0px;
                padding-bottom:10px;

            }

            /*.pagination-item-number{
                    border-radius: 25%;
                    background-color: grey;
                    
            }
            .lrn.lrn-assess .lrn-horizontal-toc ul.pagination li{
                                    border-left: 0px;
                    border-right: 0px;
                    border: none!important;
            }
            .lrn.lrn-assess .lrn-horizontal-toc ul.pagination{
                    border: 0px!important;
            }
            .lrn-horizontal-toc{
                    border-left: 0px;
                    border-right: 0px;
                    border: none!important;
            }
            .li{
                    border: none!important;
            }
            .remediationButtons{
                    height: 40px;
                    width: 130px;
                    line-height: 34px;
                    margin-top: 10px;
                    border-radius: 6px;
                    margin-right: 5px;
                    color: #fff;
                    text-transform: uppercase;
                    font-size: 12px;
                    background-color: #293644;
                    border: 1px solid #293644;
            }
            
            table, th, td {
                    table-layout:fixed;
                    width: 100%;
                    word-wrap:break-word;
            }*/

        </style>
    </head>
    <?php
    $defaultQapi = 'v2.102'
    ?>
    <body class="bi-textured">
        <div class="container">
            <div class="bborder-top">
                <div class="resource-library"><!-- BEGIN MAIN CONTAINER -->
                    <div id="msg"></div>
                    <div id="theform_div" >
                        <form action="index.php<?php
                        if (isset($_GET['QuestionsAPI'])) {
                            echo "?QuestionsAPI=" . $_GET['QuestionsAPI'];
                        }
                        ?>" id="theform" method="PUT">
                            <input type="radio" name="itemState" value="review" <?php
                            if (!isset($_GET['itemState']) or ( $_GET['itemState'] != 'initial')) {
                                echo "checked";
                            }
                            ?> >See Answer(s)
                            <input type="radio" name="itemState" value="initial" <?php
                            if (isset($_GET['itemState']) and ( $_GET['itemState'] == 'initial')) {
                                echo "checked";
                            }
                            ?> >Try Question(s)<br/>
                            <label for="item_ref">Multiple items may be entered. They must be valid and separated by a comma.</label><br/>
                            <textarea rows="4" cols="50" type="text" class="field mb1 mt1" name="item_ref" id="item_ref" 
                                      placeholder="Learnosity Problem ID" autofocus required><?php if (isset($_GET['item_ref'])) echo $_GET['item_ref']; ?> </textarea>
                            <br/>
                            <input type="submit" id="problem_search_submit" value="Get Problem(s)" class="btn mb1 mt1 btn-primary bg-teal"><br/><br/>
                            <div ><div id = "changeApi" class="btn mb1 mt1 btn-primary">Change Api (using <?php
                                    if (isset($_GET['qApi'])) {
                                        echo $_GET['qApi'];
                                    } else
                                        echo $defaultQapi;
                                    ?> )
                                </div>
                                <div id="quesApi" style="clear:both; display:none"><br/><b>Default Api: <?php echo $defaultQapi; ?></b><br/><b>Questions API: </b><input type="text" class="feld mb1 mt1" name="qApi" id="qApi" 
                                    <?php
                                    if (isset($_GET['qApi'])) {
                                        echo " value = '" . $_GET['qApi'] . "'";
                                    } else
                                        echo " value = '" . $defaultQapi . "'";
                                    ?>  >&nbsp;
                                </div><br/><br/>
                                Width (currently using: <?php echo $width; ?>) : <input type="text" class="feld mb1 mt1" name="width" id="width" ></br>
                                Height(currently using: <?php echo $height; ?>): <input type="text" class="feld mb1 mt1" name="height" id="height" ></br>
                                <input type="submit" id="problem_search_submit2" value="Update" class="btn mb1 mt1 btn-primary bg-teal"><br/>
                            </div>
                        </form>
                    </div>
                    <button id="toggleTop" style="display:none">Toggle Player Header</button>



                    <?php
                    if (isset($_GET['qApi']) && !empty($_GET['qApi'])) {
                        $qApi = $_GET['qApi'];
                    } else
                        $qApi = $defaultQapi;
                    if (isset($_GET['item_ref']) && !empty($_GET['item_ref'])) {

                        $item_ref = explode(",", $_GET['item_ref']); //allow for csv entries
                        for ($i = 0; $i < sizeof($item_ref); $i++) {
                            $item_ref[$i] = trim(trim($item_ref[$i], ' '), "\"");
                        }
                        if (isset($_GET['itemState']) && !empty($_GET['itemState'])) {
                            $itemState = $_GET['itemState'];
                        } else
                            $itemState = 'review';

                        if (isset($_GET['session']) && !empty($_GET['session'])) {
                            $session = $_GET['session'];
                        } else
                            $session = Uuid::generate();

                        $security = array(
                            "consumer_key" => $k8consumer_key,
                            "domain" => $domain,
                            'timestamp' => $timestamp
                        );
                        $prefix = '';
                        if (isset($_GET['prefix'])) {
                            $prefix = trim($_GET['prefix']);
                        }
                        $student = array(
                            'id' => 'testUser2',
                            'name' => 'Test User'
                        );
                        $request = array(
                            'activity_id' => substr($prefix . $domain . "itemsassessdemo2", 0, 36),
                            'name' => 'live progress test',
                            'rendering_type' => 'assess',
                            'state' => $itemState,
                            'type' => $itemState == 'review' ? 'local_practice' : 'submit_practice',
                            'course_id' => 'testCourse',
                            'session_id' => Uuid::generate(),
                            'user_id' => $student['id'],
                            "items" => $item_ref,
                            "retrieve_tags" => true,
                            "config" => [
                                "questions_api_init_options" => [
                                    "math_renderer" => "mathquill"
                                ],
                                "title" => "Assess Player Tests",
                                "navigation" => [
                                    "show_intro" => false,
                                    "skip_submit_confirmation" => false
                                ],
                                "labelBundle" => [
                                    "confirm" => "Yes, submit",
                                    "decline" => "Go back"
                                ]
                                , "regions" => [
                                    "items" => [
                                        [
                                            "type" => "slider_element",
                                            "vertical_stretch_option" => true
                                        ]
                                    ],
                                    "top-right" => [
                                        [
                                            "type" => "reviewscreen_button"
                                        ],
                                        [
                                            "type" => "flagitem_button"
                                        ],
                                        [
                                            "type" => "dropdown_element",
                                            "buttons" => [
                                                [
                                                    "type" => "accessibility_button"
                                                ],
                                                [
                                                    "type" => "calculator_button"
                                                ],
                                            ]
                                        ],
                                        [
                                            "type" => "fullscreen_button"
                                        ],
                                    ],
                                    "top-left" => [
                                        [
                                            "type" => "title_element"
                                        ]
                                    ],
                                    "bottom" => [
                                        [
                                            "type" => "horizontaltoc_element",
                                        ],
                                        [
                                            "type" => "previous_button",
                                            "position" => "left"
                                        ],
                                        [
                                            "type" => "next_button",
                                            "position" => "left"
                                        ],
                                        [
                                            "type" => "custom_button",
                                            "options" => ["name" => "remediation", "label" => "Remediation"],
                                            "position" => "left"
                                        ]
                                    ]
                                ], "instant_feedback" => true, /* "configuration"=>["onsubmit_redirect_url"=>"index.php"] */
                            ]
                        );
                        if ($itemState != "review") {
                            $request["config"]["regions"]["bottom"][] = [
                                "type" => "custom_button",
                                "options" => ["name" => "checkAnswer", "label" => "Check Answer"],
                                "position" => "left"
                            ];
                        }

                        $Init = new Init('items', $security, $k8consumer_secret, $request);
                        $signedRequest = $Init->generate();
                        ?>
                        <script>
                            var holdHeight = 0;
                            itemMetaData = {};
                            itemTags = {};
                            var eventOptions = {
                                readyListener: function (e) {
                                    /*prepare to generate clickable audio links for text in learnisity items*/
                                    var parser = new LearnosityAudioParser();
                                    parser.seek_all_text_nodes(true);                  // we want to drill into all nested elements within a desired class
                                    parser.append_controls(audio_element);      //add an audio element to the page, can call with 'language_toggle' to add a language selector to the page
                                    parser.setTarget('lrn_stimulus_content', 'lrn_sharedpassage');


                                    /*make sure math quill has finished rendering before we attempt text parsing*/

                                    if ($(document).find(".lrn_math_static").length > 0) {
                                        var waiting_for_mathquill = setInterval(function () {
                                            if ($(".lrn_math_static ").hasClass("lrn_math_renderer_processed")) {
                                                clearInterval(waiting_for_mathquill);
                                                parser.render_playback_links();     //gather text & create audio playback link
                                            } else {
                                                //wait
                                            }
                                        }, 50);

                                    } else {
                                        /*NO MATHQUILL ON PAGE, go straigth to parsing text */
                                        parser.render_playback_links(); //gather text & create audio playback link
                                    }

                                    $("#msg").removeClass().addClass("green");
                                    $("#msg").html("Learnosity Items API is ready");



    <?php
    if ($itemState == "initial") {
        echo "$('#validatequestions').css('display', 'inline-block');";
    }
    ?>

                                    itemMetaData = itemsApp.getItemsMetadata();
                                    itemTags = itemsApp.getTags();
                                    $("button[title='Remediation']").click(function () {
                                        var curItm = itemsApp.getCurrentItem().reference;
                                        alert("here's metadata for " + curItm + ":\n" + JSON.stringify(itemMetaData[curItm]) + "\nHere's the tags for the item:\n" + JSON.stringify(itemTags[curItm]))
                                    });
                                    $("button[title='Check Answer']").click(function () {
                                        var curItm = itemsApp.getCurrentItem();
                                        $.each(curItm["response_ids"], function (response_idx, response_id) {
                                            itemsApp.question(response_id).validate({"showCorrectAnswers": true});
                                        });
                                    });
                                    //$('.slides-container.scrollable.vertical-stretch').height(<?php echo $height; ?>);
                                    $('#toggleTop').show();
                                    var origTopHeight = $('.lrn-top-left-region').height();
                                    $('#toggleTop').click(function () {
                                        if ($('.lrn-top-left-region').is(':visible')) {
                                            $('.slides-container.scrollable.vertical-stretch').height($('.slides-container.scrollable.vertical-stretch').height() + origTopHeight);
                                        } else {
                                            $('.slides-container.scrollable.vertical-stretch').height($('.slides-container.scrollable.vertical-stretch').height() - origTopHeight);
                                        }
                                        $('.lrn-top-left-region').toggle();
                                        $('.lrn-top-right-region').toggle();
                                    });
                                    $('#toggleBottom').show();
                                    var origBottomHeight = $('.lrn-bottom-region').height();
                                    $('#toggleBottom').click(function () {
                                        if ($('.lrn-bottom-region').is(':visible')) {
                                            $('.slides-container.scrollable.vertical-stretch').height($('.slides-container.scrollable.vertical-stretch').height() + origBottomHeight);
                                        } else {
                                            $('.slides-container.scrollable.vertical-stretch').height($('.slides-container.scrollable.vertical-stretch').height() - origBottomHeight);
                                        }
                                        //$('.lrn-bottom-region').css('background-color','white');
                                        $('.lrn-bottom-region').toggle();
                                        $('.lrn-region.lrn-bottom-left-region').toggle();
                                        $('.lrn-region.lrn-bottom-right-region').toggle();
                                    });
                                    $('.lrn_btn.test-fullscreen-btn').click(function () {
                                        $('.lrn-bottom-region').show();
                                        $('.lrn-region.lrn-bottom-left-region').show();
                                        $('.lrn-region.lrn-bottom-right-region').show();
                                    });
                                },
                                errorListener: function (e) {
                                    // Adds a listener to all error
                                    console.log(e);
                                    $("#msg").html('');
                                    $("#msg").removeClass().addClass("red");
                                    //$("#msg").html("Error: "+JSON.stringify(e));
                                    $("#msg").html("Error: " + e["msg"]);
                                }
                            };
                            //console.log('<?php //echo $signedRequest;     ?>');
                            var itemsApp = LearnosityItems.init(<?php echo $signedRequest; ?>, eventOptions);
                            $('#theproblem').html(itemsApp + "<br>" + JSON.stringify(itemsApp));
                            //$('#title').html("<?php echo implode(",", $item_ref); ?>");
                            var display = false;
                            $('#changeApi').click(function () {
                                if (display === true) {
                                    $("#quesApi").hide();
                                    display = false;
                                } else if (display === false) {
                                    $("#quesApi").show();
                                    display = true;
                                }
                                //$( "#quesApi" ).toggle()( "slow", function() {
                                // Animation complete.
                                //});
                            });

                        </script>
                        <div id="viewWindow">
                            <div id="learnosity_assess"></div>
                        </div>
                        <button id="toggleBottom" style="display:none">Toggle Player Footer</button>
                        <br>
                        <div id="bottomNavOut" style="display:none"></div>
                    </div></div></div><!-- END MAIN -->

            <?php
            if ($itemState != 'review') {
                echo '<script>
var submitSettings = {
    success: function (response_ids) {
        // Receives a list of the submitted user responses as [response_id]
        console.log("submit has been successful", response_ids);
    },
    error: function (e) {
        // Receives the event object defined in the Event section
        console.log("submit has failed",e);
    },
    progress: function (e) {
        // Client custom progress event handler
        // e: uses the progress object defined bellow
        // See the Progress Event section for more information on this
        console.log("progress",e);
    }
    
};


$("#SubmitBtn").click(function(){itemsApp.submit(submitSettings);});</script>';
            }
        }//END IF
        ?>

    </body>
</html>
