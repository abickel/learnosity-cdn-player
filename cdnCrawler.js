var cdn_crawler = function(md5, speechText, voices, textSource) {

    var voiceArray = voices,
        hash = md5,
        hashDir = md5[0] + md5[1] + md5[2],
        text = speechText,
        source = textSource,
        url = "",
        index = 0,
        baseUrl = "http://68b9c1bc7c9fa35e4a6d-0264cef6aad1b81efe617b95ad9e9064.r0.cf2.rackcdn.com",
        audioPlayerId = "audio_player",
        language = "",

        setLanguage = function(lang) {
            language = lang;
        },
        seekFile = function() {


            if (index != voiceArray.length) { //try a file location

                var url = baseUrl + "/" + hashDir + "/" + voiceArray[index] + "/" + hash + ".mp3";
                $.ajax({
                    url: url,
                    type: 'head',
                    md5: hash,
                    vid: voiceArray[index],
                    success: function(response) {

                        loadAudio(audioPlayerId, { "url": url, "voice_id": voiceArray[index], "md5": hash });
                    },
                    /*search for this index failed, try next index in array */
                    error: function(response) {
                        index += 1;

                        seekFile(); //check next index in array 
                    }
                });


            } else {
                check_tts_directory();
            }

        },
        loadAudio = function(audioPlayerId, data) {
            debugger;
            console.log(data);
            var player = $("#" + audioPlayerId);
            var mp3 = document.createElement("source");
            mp3.src = data.url;
            mp3.dataset.md5 = data.md5;
            mp3.dataset.voice_id = data.voice_id;

            player[0].pause(); //in case of latent play state
            player.empty(); //clear player sources
            player.append(mp3); //add new source
            player.load(); //restart
            player.trigger("play"); //play audio

        },
        check_tts_directory = function() {

            /**1. check TTS directory */
            var ttsUrl = baseUrl + "/" + hashDir + "/tts/" + language + "/" + hash + ".mp3";

            $.ajax({
                url: ttsUrl,
                type: 'head',
                md5: hash,
                lang: language,
                success: function(response, language, hash) {

                    //console.log(response);
                    //var data = JSON.parse(response);

                    loadAudio(audioPlayerId, { "url": ttsUrl, "voice_id": language, "md5": hash });

                },
                /*search for tts failed, need to get audio from polly, as well as log the missing file to database */
                error: function(response) {

                    log_missing_file_to_database(); //note the missing file

                    //retrieve_polly_recording(); //hook to AWS polly to generate TTS, push to CDN, then return url to client


                }
            });
        },
        log_missing_file_to_database = function() {
            /**update database with record of this missing file */
            var payload = { "log_missing_file": "learnosity", "md5": hash, "text": text, "activity": source, "voiceID": voiceArray[0] };

            $.post("http://www-staging.bigideasmath.com/assessmentSandbox/cdn-player/cdn-services/index.php", payload, function(response) {
                retrieve_polly_recording();
            });
        },
        retrieve_polly_recording = function() {

            var payload = { "request_polly_audio": "learnosity", "md5": hash, "text": text, "language": language };

            $.post("http://www-staging.bigideasmath.com/assessmentSandbox/cdn-player/cdn-services/index.php", payload, function(response) {
                var baseUrl = "http://68b9c1bc7c9fa35e4a6d-0264cef6aad1b81efe617b95ad9e9064.r0.cf2.rackcdn.com";
                var data = JSON.parse(response);
                console.log(data);
                var url = baseUrl + "/" + data.filename;
                loadAudio(audioPlayerId, { "url": url, "voice_id": language, "md5": hash });
            });

        };

    return {
        seekFile: seekFile,
        setLanguage: setLanguage
    };
};