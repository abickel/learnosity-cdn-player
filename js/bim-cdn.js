/**
 * in readyListener definition
 * 
 *var parser = new LearnosityAudioParser();
  parser.seek_all_text_nodes(true);                                  <-- define seek depth  true | false 
  parser.append_controls(audio_element);                             <--  trigger the creation of audio elemen to play mp3s
  parser.setTarget('lrn_stimulus_content', 'lrn_sharedpassage');     <-- define which learnosity areas areas we want provide audio for
                                    
  if ($(document).find(".lrn_math_static").length > 0) {
      var waiting_for_mathquill = setInterval(function () {
          if ($(".lrn_math_static ").hasClass("lrn_math_renderer_processed")) {
              clearInterval(waiting_for_mathquill);
              parser.render_playback_links();     //gather text & create audio playback link
          } else {
              //wait
          }
      }, 50);

   } else {
       /*NO MATHQUILL ON PAGE, go straigth to parsing text 
     parser.render_playback_links(); //gather text & create audio playback link
   } 
   */


/**class LearnosityAudioParser - handle dynamic retrival/ generation of TTS audio, served from rackspace CDN */
var LearnosityAudioParser = function() {

    /*options */
    var config = {
            "seek_all_text": true,
            "opts": []
        },
        /**wchich element classes do we want to parse
         * values passed act as callback trigger - do not pass args as sting.  
         */
        setTarget = function(...args) {
            for (var i in args) {
                config.opts.push(args[i]);
            }
        },
        /** parse only First Child( false) or all (true) through the target object
         */
        seek_all_text_nodes = function(state) {
            config.seek_all_text = state;
        },
        /*addaudio element to page   -  @param control =  {audio_element} */
        append_controls = function(control) {
            control();
        },
        /**call each 'target' function that client has specified */
        render_playback_links = function() {
            for (var i in config.opts) {
                var callback = config.opts[i];
                this[callback]();
            }
        },
        /**find text within DOM elements containing 'lrn_stimulus_content' class */
        lrn_stimulus_content = function() {
            $(".lrn_stimulus_content").each(function(i, e) {
                var item = $(this);
                $(this).parents().each(function() { //seek activity source
                    if ($(this).data("reference")) {
                        text_locate(item, $(this).data("reference"), "lsc_" + i);
                    }
                });
            });
        },
        /**find text within DOM elements containing 'lrn_sharedpassage' class */
        lrn_sharedpassage = function() {
            $(".lrn_sharedpassage").each(function(i, e) {
                var item = $(this);
                $(this).parents().each(function() { //seek activity source
                    if ($(this).data("reference")) {
                        text_locate(item, $(this).data("reference"), "lsp" + i);
                    }
                });
            });
        },

        /**search for desired text nodes within the passed object (obj)
         * reference = source activity for this string
         * ixd  = a unique identigier
         */
        text_locate = function(obj, reference, idx) {

            if ((obj.children().length === 0) && (element_has_desired_class(obj))) { //if bare text node found, is this one of the elements we want to display?

                insert_audio_playback(obj, reference, idx);
                return;

            } else {

                if (config.seek_all_text) { //find all text nodes in this object?

                    obj.children().each(function(i, e) {
                        //if child IS a<p> element
                        if ($(this).is("p")) {
                            insert_audio_playback($(this), reference, idx + "-" + i);
                        } else {
                            //if child CONTAINS a<p> element
                            $(this).find("p").each(function() {
                                insert_audio_playback($(this), reference, idx + "-" + i);
                            });
                        }
                    });
                } else { //only add audio to first text node found

                    /**not tagging all the text in this element, only the first on we come across */
                    var target = obj.find("p").eq(0);
                    insert_audio_playback(target, reference, idx);
                }
            }

        },

        insert_audio_playback = function(trgt, reference, idx) {

            /**clean up text input */
            var text = sanitize_mathquill(trgt, reference); //re-form object text if mathquill syntax found,
            var filter_safe_text = handle_errant_math_symbols(text); //deal with miscellaneous html representation of math symbols (that may get flushed by server sanitization)
            var clean_text = cleanText(filter_safe_text);

            if (clean_text === "")
                return;


            /*create GUID */
            var md5 = MD5(clean_text),
                info = { "md5": md5, "text": clean_text, "source": reference, "index": idx },
                button = $('<img id="playback_' + idx + '" class="audio-play" src="learnosity-player-images/speaker.png" alt="a picture of a speaker. Click to read the question out loud"></img>');
            button.data("info", info); //store informatin about text string in case we need to send to Polly for TTS generation
            button.on('click', function() { retrieveAudio(idx) });
            if ((trgt.has("img")) && (text.length < 2)) // do not add text for single letter (or single space) strings
                return;

            trgt.append(button);
        },

        /**determine if a given dom element matches the classes provided in config options */
        element_has_desired_class = function(obj) {
            for (var i in config.opts) {
                var listing = config.opts[i];
                if (obj.hasClass(listing))
                    return true;
                return false;
            }
        };

    return {
        setTarget: setTarget,
        seek_all_text_nodes: seek_all_text_nodes,
        append_controls: append_controls,
        render_playback_links: render_playback_links,
        lrn_stimulus_content: lrn_stimulus_content,
        lrn_sharedpassage: lrn_sharedpassage
    };

}

/************* audio | utility | DOM manipulation **********/

/*create media player for audio playback */
function audio_element() {
    var player = $('<audio id="audio_player"></audio>');
    $("body").append(player);
}

/**attempt to retieve mp3 from CDN */
function retrieveAudio(index) {

    var elem = $("#playback_" + index),
        data = elem.data("info"),
        md5 = data.md5,
        cdn_location = "/" + md5[0] + md5[1] + md5[2] + "/tts/e/" + md5 + ".mp3",
        baseurl = "http://68b9c1bc7c9fa35e4a6d-0264cef6aad1b81efe617b95ad9e9064.r0.cf2.rackcdn.com";

    $.ajax({
        url: baseurl + cdn_location,
        type: 'head',
        text: data.text
    }).done(function() {
        loadAudio(baseurl + cdn_location);
    }).fail(function() {
        retrieve_poly_audio(data);
    });

}

/**push a verified url to the audio player, trigger playback */
function loadAudio(url) {
    var player = $("#audio_player"),
        mp3 = $('<source src="' + url + '"></source>');
    player[0].pause(); //in case of latent play state
    player.empty(); //clear player sources
    player.append(mp3); //add new source
    player[0].load(); //restart
    player.trigger("play"); //play audio
}

/**tts retrival failed, need to request a new fille from polly & store it on the cdn */
function retrieve_poly_audio(data) {
    var md5 = data.md5,
        text = data.text;

    var cdn_location = "/" + md5[0] + md5[1] + md5[2] + "/tts/e/" + md5 + ".mp3",
        baseurl = "http://68b9c1bc7c9fa35e4a6d-0264cef6aad1b81efe617b95ad9e9064.r0.cf2.rackcdn.com",
        payload = { "hash": md5, "text": text, "action": "learnosity_tts_audio_only" };
    $.ajax({
        url: "http://www-staging.bigideasmath.com/assessmentSandbox/cdn-player/cdn/index.php",
        method: "post",
        data: payload
    }).done(function(response) {

        data = JSON.parse(response);

        loadAudio(baseurl + "/" + data.filename);

    }).fail(function(response) {
        console.log("Server failure");
        console.log(response.error());
    });
}
/**detect the presence of mathquill syntax in an element, and extract polly-readable speech text */
function sanitize_mathquill($elem, reference) {

    var $copy = $elem.clone(),
        results = {};

    /*no mathquill found, return original text */
    if ($copy.find("span .lrn_math_renderer_processed").length === 0) {
        return $elem.text();
    }

    $copy.find("span .lrn_math_renderer_processed").each(function() {

        var $tts = $(this).find(".lrn-display-offscreen");
        $(this).text($tts.text());
    });
    return $copy.text();
}

/*strip  big spaces from text string */
function cleanText(input) {
    var parsed = input.replace(/\u00A0/g, ' '); //kill hardcoded unicode-encoded(&nbsp;) spaces
    parsed = parsed.replace(/\s/g, ' '); //html encoded (&nbsp;) spaces
    parsed = parsed.replace('&nbsp;', ' ');
    return parsed.replace(/\s{2,}/g, ' ').trim(); //trim leading and trailing spaces
}

/*misc. math functions written as raw HTML - replace with text before sending to server */
function handle_errant_math_symbols(text) {

    var new_text = text.replace(/</g, ' less than ');
    new_text = new_text.replace(/>/g, ' greater than ');
    new_text = new_text.replace(/=/g, ' equals ');
    new_text = new_text.replace(/\+/g, ' plus ');
    return new_text;
}



/*hashing functions*/
/**
 *
 *  MD5 (Message-Digest Algorithm)
 *  http://www.webtoolkit.info/
 *
 **/
/**
 *
 *  MD5 (Message-Digest Algorithm)
 *  http://www.webtoolkit.info/
 *
 **/
var MD5 = function(string) {
    function RotateLeft(lValue, iShiftBits) {
        return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
    }

    function AddUnsigned(lX, lY) {
        var lX4, lY4, lX8, lY8, lResult;
        lX8 = (lX & 0x80000000);
        lY8 = (lY & 0x80000000);
        lX4 = (lX & 0x40000000);
        lY4 = (lY & 0x40000000);
        lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
        if (lX4 & lY4) {
            return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
        }
        if (lX4 | lY4) {
            if (lResult & 0x40000000) {
                return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
            } else {
                return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
            }
        } else {
            return (lResult ^ lX8 ^ lY8);
        }
    }

    function F(x, y, z) { return (x & y) | ((~x) & z); }

    function G(x, y, z) { return (x & z) | (y & (~z)); }

    function H(x, y, z) { return (x ^ y ^ z); }

    function I(x, y, z) { return (y ^ (x | (~z))); }

    function FF(a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function GG(a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function HH(a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function II(a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function ConvertToWordArray(string) {
        var lWordCount;
        var lMessageLength = string.length;
        var lNumberOfWords_temp1 = lMessageLength + 8;
        var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
        var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
        var lWordArray = Array(lNumberOfWords - 1);
        var lBytePosition = 0;
        var lByteCount = 0;
        while (lByteCount < lMessageLength) {
            lWordCount = (lByteCount - (lByteCount % 4)) / 4;
            lBytePosition = (lByteCount % 4) * 8;
            lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount) << lBytePosition));
            lByteCount++;
        }
        lWordCount = (lByteCount - (lByteCount % 4)) / 4;
        lBytePosition = (lByteCount % 4) * 8;
        lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
        lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
        lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
        return lWordArray;
    };

    function WordToHex(lValue) {
        var WordToHexValue = "",
            WordToHexValue_temp = "",
            lByte, lCount;
        for (lCount = 0; lCount <= 3; lCount++) {
            lByte = (lValue >>> (lCount * 8)) & 255;
            WordToHexValue_temp = "0" + lByte.toString(16);
            WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
        }
        return WordToHexValue;
    };

    function Utf8Encode(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    };
    var x = Array();
    var k, AA, BB, CC, DD, a, b, c, d;
    var S11 = 7,
        S12 = 12,
        S13 = 17,
        S14 = 22;
    var S21 = 5,
        S22 = 9,
        S23 = 14,
        S24 = 20;
    var S31 = 4,
        S32 = 11,
        S33 = 16,
        S34 = 23;
    var S41 = 6,
        S42 = 10,
        S43 = 15,
        S44 = 21;
    string = Utf8Encode(string);

    string = sanitize(string);

    x = ConvertToWordArray(string);
    a = 0x67452301;
    b = 0xEFCDAB89;
    c = 0x98BADCFE;
    d = 0x10325476;
    for (k = 0; k < x.length; k += 16) {
        AA = a;
        BB = b;
        CC = c;
        DD = d;
        a = FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
        d = FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
        c = FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
        b = FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
        a = FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
        d = FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
        c = FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
        b = FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
        a = FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
        d = FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
        c = FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
        b = FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
        a = FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
        d = FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
        c = FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
        b = FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
        a = GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
        d = GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
        c = GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
        b = GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
        a = GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
        d = GG(d, a, b, c, x[k + 10], S22, 0x2441453);
        c = GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
        b = GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
        a = GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
        d = GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
        c = GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
        b = GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
        a = GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
        d = GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
        c = GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
        b = GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
        a = HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
        d = HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
        c = HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
        b = HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
        a = HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
        d = HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
        c = HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
        b = HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
        a = HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
        d = HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
        c = HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
        b = HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
        a = HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
        d = HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
        c = HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
        b = HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
        a = II(a, b, c, d, x[k + 0], S41, 0xF4292244);
        d = II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
        c = II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
        b = II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
        a = II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
        d = II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
        c = II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
        b = II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
        a = II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
        d = II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
        c = II(c, d, a, b, x[k + 6], S43, 0xA3014314);
        b = II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
        a = II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
        d = II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
        c = II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
        b = II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
        a = AddUnsigned(a, AA);
        b = AddUnsigned(b, BB);
        c = AddUnsigned(c, CC);
        d = AddUnsigned(d, DD);
    }
    var temp = WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d);
    return temp.toLowerCase();
}




var sanitize = function(string) {
    var str = trim(string);
    //Using http://unicode-search.net/unicode-namesearch.pl
    //replace all unicode dash (minus) types with "hyphen-minus" '-'
    str = str.replace(/[\u00AD\u05BE​\u1806\u2010\u2011\u2012​\u2013\u2014\u2015\u2212\uFE58\uFE63\uFF0D]/g, '\u002D');
    //replace Full-width Plus with Plus
    str = str.replace(/[\uFF0B\u2795]/g, '\u002b');
    //replace versions of multiplication signs with
    str = str.replace(/[\u2715\u2716]/g, '\u00D7');
    //replace double quotes with standard double quotes
    str = str.replace(/[\u201C\u201D\u201F\u275D\u275D\u275E\u301D\u301E\uFF02\u2033]/g, '\u0022');
    //replace double spaces with single spaces
    str = str.replace(/\s\s+/g, ' ');
    //replace curly apostrophes and prime with apostrophes
    str = str.replace(/[\u02BC\u055A\u07F4\u07F5\uFF07\u2018\u2019\u201B\u275B\u275C\u2032]/g, '\u0027');
    //replace elipses with ...
    str = str.replace(/[\u2026]/g, '...');
    //replace i acute with i
    str = str.replace(/[\u00CD\u00ED]/g, 'i');
    //replace Combining Left Arrow above with left arrow
    str = str.replace(/[\u20D6]/g, '\u2190');
    //replace Combining Right Arrow above with right arrow
    str = str.replace(/[\u20D7]/g, '\u2192');
    //replace left-pointing angle bracket with <
    str = str.replace(/[\u2329\u27E8\u3008]/g, '<');
    //replace right-pointing angle bracket with >
    str = str.replace(/[\u232A\u27E9\u3009]/g, '>');
    //WE MAY OR MAY NOT WANT TO DO THIS

    str = removePrivateUse(str);
    return str;
}

var removePrivateUse = function(str) {
    //converts all private use unicode characters to '<|pu|>'
    //replace private use with <pu>
    s = str.replace(/[\uE000-\uF8FF]/g, '<|pu|>');
    return s;
}


var trim = function(string) {
    return string.replace(/^\s+|\s+$/gm, '');
}